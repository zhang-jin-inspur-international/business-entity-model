/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.element.GspAssociationKey;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Parser Of GspAssoKey
 *
 * @ClassName: GspAssoKeyDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssoKeyDeserializer extends JsonDeserializer<GspAssociationKey> {

    public GspAssoKeyDeserializer() {
    }

    @Override
    public GspAssociationKey deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return deserializeGspAssociation(jsonParser);
    }

    public GspAssociationKey deserializeGspAssociation(JsonParser jsonParser) {
        GspAssociationKey associationKey = new GspAssociationKey();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(associationKey, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return associationKey;
    }

    private void readPropertyValue(GspAssociationKey associationKey, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.SourceElement:
                associationKey.setSourceElement(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.TargetElement:
                associationKey.setTargetElement(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.SourceElementDisplay:
                associationKey.setSourceElementDisplay(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.TargetElementDisplay:
                associationKey.setTargetElementDisplay(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.RefDataModelName:
            case CefNames.RefdataModelName:
                associationKey.setRefDataModelName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
        }
    }
}
