/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.validate.element;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class FieldChecker {
    protected boolean isLegality(String code) {
        return CheckUtil.isLegality(code);
    }

    public final void checkField(IGspCommonField field, IGspCommonDataType commonDataType) {
        checkBaseFieldInfo(field, commonDataType);
        checkFieldWithObjectType(field, commonDataType);
        checkFieldExtension(field);
    }

    protected void checkFieldExtension(IGspCommonField field) {

    }

    private void checkFieldWithObjectType(IGspCommonField field, IGspCommonDataType commonDataType) {
        if (field == null) return;
        switch (field.getObjectType()) {
            case Association:
                AssociationFieldChecker.getInstance().checkAssociationField(field, commonDataType);
                break;
            case Enum:
                EunmFieldChecker.getInstance().checkEunm(field, commonDataType);
                break;
            case None:
                break;
            case DynamicProp:
                break;
            default:
                break;
        }
    }

    private void checkBaseFieldInfo(IGspCommonField field, IGspCommonDataType gspCommonDataType) {
        if(field.getLabelID().isEmpty()){
            CheckUtil.exception("节点[" + gspCommonDataType.getCode() + "]的字段["+field.getName()+"]标签不允许为空,请修改！");
        }
        if (!isLegality(field.getLabelID())) {
            CheckUtil.exception("节点[" + gspCommonDataType.getCode() + "]的字段标签[" +field.getLabelID()+"]是Java关键字,请修改！");
        }
        checkBaseExtension();
    }

    protected void checkBaseExtension() {

    }
}
