/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleNames;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.RangeControlRule;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.fasterxml.jackson.core.JsonToken.END_ARRAY;

/**
 * The Json Parser Of RangeRule
 *
 * @ClassName: RangeRuleParser
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class RangeRuleParser<T extends RangeControlRule> extends AbstractControlRuleParser<T> {

    @Override
    protected final boolean readExtendControlRuleProperty(T controlRule, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readExtendControlRuleProperty(controlRule, propName, jsonParser, deserializationContext))
            return true;
        if (propName.equals(ControlRuleNames.RangeControlRules)) {
            SerializerUtils.readStartObject(jsonParser);
            innerReadRangeRules(controlRule, jsonParser, deserializationContext);
            SerializerUtils.readEndObject(jsonParser);
            return true;
        }
        return readRangeExtendControlRuleProperty(controlRule, propName, jsonParser, deserializationContext);
    }

    protected boolean readRangeExtendControlRuleProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    private void innerReadRangeRules(T ruleDefinition, JsonParser jsonParser, DeserializationContext deserializationContext) {
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String properyName = SerializerUtils.readPropertyName(jsonParser);
            Map<String, ControlRuleItem> items = readRangeRuleItems(jsonParser, deserializationContext);
            ruleDefinition.getRangeControlRules().put(properyName, items);
        }
    }

    private Map<String, ControlRuleItem> readRangeRuleItems(JsonParser jsonParser, DeserializationContext deserializationContext) {
        Map<String, ControlRuleItem> map = new HashMap<>();
        SerializerUtils.readStartArray(jsonParser);
        JsonToken tokentype = jsonParser.getCurrentToken();
        if (tokentype != END_ARRAY) {
            while (jsonParser.getCurrentToken() == tokentype) {
                try {
                    ControlRuleItem item = SerializerUtils.readPropertyValue_Object(ControlRuleItem.class, jsonParser);
                    map.put(item.getRuleName(), item);
                    jsonParser.nextToken();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        SerializerUtils.readEndArray(jsonParser);
        return map;
    }

    @Override
    protected T createControlRule() {
        return (T) new RangeControlRule();
    }

    @Override
    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        return null;
    }
}
