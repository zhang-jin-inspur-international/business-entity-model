/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.parser.AbstractControlRuleParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonFieldControlRule;

/**
 * The Json Parser Of CommonFieldRule
 *
 * @ClassName: CommonFieldRuleParser
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonFieldRuleParser<T extends CommonFieldControlRule> extends AbstractControlRuleParser<T> {
    @Override
    protected final boolean readExtendControlRuleProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readExtendControlRuleProperty(ruleDefinition, propName, jsonParser, deserializationContext))
            return true;
        return readCommonFieldRuleExtendProperty(ruleDefinition, propName, jsonParser, deserializationContext);
    }

    protected boolean readCommonFieldRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    @Override
    protected T createControlRule() {
        return (T) createCommonFieldRuleDefinition();
    }

    @Override
    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        return null;
    }

    protected CommonFieldControlRule createCommonFieldRuleDefinition() {
        return new CommonFieldControlRule();
    }
}
