/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.operation;

public enum CommonTriggerPointType {
    /**
     不执行,用于判断时机比较结果

     */
    None(0),

    /**
     数据更新后
     可以操作持久化或非持久化属性和Node

     */
    AfterModify(1),

    /**
     一致性检查前，保存中进行Validation之前进行，是修改数据的最后时机

     */
    BeforeSave(2);

    private int intValue;
    private static java.util.HashMap<Integer, CommonTriggerPointType> mappings;
    private synchronized static java.util.HashMap<Integer, CommonTriggerPointType> getMappings()
    {
        if (mappings == null)
        {
            mappings = new java.util.HashMap<Integer, CommonTriggerPointType>();
        }
        return mappings;
    }

    private CommonTriggerPointType(int value)
    {
        intValue = value;
        CommonTriggerPointType.getMappings().put(value, this);
    }

    public int getValue()
    {
        return intValue;
    }

    public static CommonTriggerPointType forValue(int value)
    {
        return getMappings().get(value);
    }
}
