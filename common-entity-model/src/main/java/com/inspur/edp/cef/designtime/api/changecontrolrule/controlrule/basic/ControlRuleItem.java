/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.parser.ControlRuleItemParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.serializer.ControlRuleItemSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;

/**
 * The Definition Of ControlRuleItem
 *
 * @ClassName: ControlRuleItem
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = ControlRuleItemSerializer.class)
@JsonDeserialize(using = ControlRuleItemParser.class)
public class ControlRuleItem {
    private String ruleName;
    private ControlRuleValue controlRuleValue = ControlRuleValue.Default;

    //region 规则名称
    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
    //endregion

    //region 规则
    public ControlRuleValue getControlRuleValue() {
        return controlRuleValue;
    }

    public void setControlRuleValue(ControlRuleValue controlRuleValue) {
        this.controlRuleValue = controlRuleValue;
    }
    //endregion
}
