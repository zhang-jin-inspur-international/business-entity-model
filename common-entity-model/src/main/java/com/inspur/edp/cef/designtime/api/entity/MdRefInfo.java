/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MdRefInfo implements Cloneable {
	/**
	 * 元数据包名
	 * 
	 */
	private String privatePkgName;

	@JsonProperty("PkgName")
	public final String getPkgName() {
		return privatePkgName;
	}

	public final void setPkgName(String value) {
		privatePkgName = value;
	}

	/**
	 * 元数据Id
	 * 
	 */
	private String privateId;
	@JsonProperty("Id")
	public final String getId() {
		return privateId;
	}

	public final void setId(String value) {
		privateId = value;
	}

	/**
	 * 元数据名称
	 * 
	 */
	private String privateName;
	@JsonProperty("Name")
	public final String getName() {
		return privateName;
	}

	public final void setName(String value) {
		privateName = value;
	}

	/**
	 * 拷贝副本
	 * 
	 * @throws CloneNotSupportedException
	 */
	public MdRefInfo clone() {
		try {
			return (MdRefInfo) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}
