/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildDtmTriggerInfo;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;
import java.io.IOException;
import java.util.EnumSet;

public class ChildDtmTriggerInfoSerializer extends JsonSerializer<ChildDtmTriggerInfo> {
    protected boolean isFull = true;
    public ChildDtmTriggerInfoSerializer(){}
    public ChildDtmTriggerInfoSerializer(boolean full){
        isFull = full;
    }
    @Override
    public void serialize(ChildDtmTriggerInfo childDtmTriggerInfo, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        SerializerUtils.writeStartObject(jsonGenerator);
        if(isFull||(!childDtmTriggerInfo.getGetExecutingDataStatus().isEmpty()&&!childDtmTriggerInfo.getGetExecutingDataStatus().iterator().next().equals(ExecutingDataStatus.None))){
            SerializerUtils.writePropertyName(jsonGenerator, CefNames.GetExecutingDataStatus);
            writeGetExecutingDataStatus(jsonGenerator, childDtmTriggerInfo.getGetExecutingDataStatus());
        }
        if(isFull||(childDtmTriggerInfo.getRequestChildElements()!=null&&childDtmTriggerInfo.getRequestChildElements().size()>0))
            writeRequestChildElements(jsonGenerator, childDtmTriggerInfo.getRequestChildElements());
        SerializerUtils.writeEndObject(jsonGenerator);
    }

    protected void writeGetExecutingDataStatus(JsonGenerator writer, EnumSet<ExecutingDataStatus> value) {
        int intValue = 0;
        for (ExecutingDataStatus timePointType : value) {
            intValue += timePointType.getValue();
        }
        SerializerUtils.writePropertyValue_Integer(writer, intValue);
    }

    protected void writeRequestChildElements(JsonGenerator writer, DtmElementCollection dic) {
        if (dic != null && dic.size() > 0) {
            SerializerUtils.writePropertyName(writer, CefNames.RequestChildElements);
            writeDtmElementCollection(writer, dic);
        }
    }

    private void writeDtmElementCollection(JsonGenerator writer, DtmElementCollection childElementsIds) {
        SerializerUtils.WriteStartArray(writer);
        if (childElementsIds.size() > 0) {
            for (String item : childElementsIds) {
                SerializerUtils.writePropertyValue_String(writer, item);
            }
        }
        SerializerUtils.WriteEndArray(writer);
    }
}
