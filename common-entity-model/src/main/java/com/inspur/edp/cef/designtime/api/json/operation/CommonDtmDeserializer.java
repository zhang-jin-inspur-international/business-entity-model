/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildDtmTriggerInfo;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonTriggerPointType;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The Json Parser Of CommonDetermination
 *
 * @ClassName: CommonDtmDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDtmDeserializer extends CommonOpDeserializer {
    @Override
    protected void beforeDeserializeCommonOp(CommonOperation op) {
        CommonDetermination dtm = (CommonDetermination) op;
        dtm.setRequestElements(new DtmElementCollection());
    }

    @Override
    protected boolean readExtendOpProperty(CommonOperation op, String propName, JsonParser jsonParser) {
        boolean result = true;
        CommonDetermination dtm = (CommonDetermination) op;
        switch (propName) {
            case CefNames.GetExecutingDataStatus:
                dtm.setGetExecutingDataStatus(readGetExecutingDataStatus(jsonParser));
                break;
            case CefNames.TriggerPointType:
                dtm.setTriggerPointType(SerializerUtils
                        .readPropertyValue_Enum(jsonParser, CommonTriggerPointType.class,
                                CommonTriggerPointType.values()));
                break;
            case CefNames.RequestElements:
                dtm.setRequestElements(readRequestElements(jsonParser));
                break;
                //第一次的时候要组装到ChildTriggerInfo里面
            case CefNames.RequestChildElements:
                readRequestChildElements(dtm, jsonParser);
                break;
            case CefNames.ChildTriggerInfo:
                dtm.setChildTriggerInfo(readChildTriggerInfo(jsonParser));
                break;
            default:
                result = readExtendDtmProp(op, propName, jsonParser);
                break;
        }
        return result;
    }

    protected boolean readExtendDtmProp(CommonOperation op, String propName, JsonParser jsonParser){
        return false;
    }

    public static CommonTriggerPointType readBETriggerTimePointType(JsonParser jsonParser) {
        CommonTriggerPointType result = null;
        int intValueSum = SerializerUtils.readPropertyValue_Integer(jsonParser);
        CommonTriggerPointType[] values = CommonTriggerPointType.values();
        // 不包含none
        for (int i = values.length - 1; i > 0; i--) {
            CommonTriggerPointType value = values[i];
            if (intValueSum > 0 && intValueSum == value.getValue()) {
                result = value;
                intValueSum -= value.getValue();
            }
        }
        return result;
    }

    private DtmElementCollection readRequestElements(JsonParser jsonParser) {
        ArrayList<String> list = SerializerUtils.readStringArray(jsonParser);
        DtmElementCollection collection = new DtmElementCollection();
        for (int i = 0; i < list.size(); i++) {
            collection.add(list.get(i));
        }
        return collection;
    }

    private void readRequestChildElements(CommonDetermination dtm, JsonParser jsonParser) {
        RequestChildElementsDeserializer deserializer = new DtmRequestChildElementsDeserializer();
        HashMap<String, DtmElementCollection> childRequestELements =  deserializer.deserialize(jsonParser, null);
        dtm.setRequestChildElements(childRequestELements);
//        HashMap<String, ChildDtmTriggerInfo> childDtmTriggerInfoHashMap = new HashMap<>();
//        for(Map.Entry<String, DtmElementCollection> item : childRequestELements.entrySet()){
//            ChildDtmTriggerInfo childTriggerInfo = new ChildDtmTriggerInfo();
//            childTriggerInfo.setRequestChildElements(item.getValue());
//            EnumSet<ExecutingDataStatus> status = EnumSet.of(ExecutingDataStatus.added, ExecutingDataStatus.Deleted, ExecutingDataStatus.Modify);
//            childTriggerInfo.setGetExecutingDataStatus(status);
//            childDtmTriggerInfoHashMap.put(item.getKey(), childTriggerInfo);
//        }
//        dtm.setChildTriggerInfo(childDtmTriggerInfoHashMap);
    }

    private HashMap<String, ChildDtmTriggerInfo> readChildTriggerInfo(JsonParser jsonParser) {
        ChildDtmTriggerInfoDeserializer deserializer = new ChildDtmTriggerInfoDeserializer();
        return deserializer.deserialize(jsonParser, null);
    }

    @Override
    protected CommonOperation CreateCommonOp() {
        return new CommonDetermination();
    }
}
