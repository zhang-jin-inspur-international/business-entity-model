/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.parser.ControlDefItemParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer.ControlRuleDefItemSerializer;

/**
 * The Definition Of ControlRuleDefItem
 *
 * @ClassName: ControlRuleDefItem
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = ControlRuleDefItemSerializer.class)
@JsonDeserialize(using = ControlDefItemParser.class)
public class ControlRuleDefItem {
    private String description;
    private String ruleDisplayName;
    private String ruleName;
    private ControlRuleValue defaultRuleValue;

    public ControlRuleDefItem() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRuleDisplayName() {
        return ruleDisplayName;
    }

    public void setRuleDisplayName(String ruleDisplayName) {
        this.ruleDisplayName = ruleDisplayName;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public ControlRuleValue getDefaultRuleValue() {
        return defaultRuleValue;
    }

    public void setDefaultRuleValue(ControlRuleValue defaultRuleValue) {
        this.defaultRuleValue = defaultRuleValue;
    }
}
