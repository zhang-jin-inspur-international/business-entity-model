/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.element.increment.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.increment.AddedFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.DeletedFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.GspCommonFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.ModifyFieldIncrement;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.json.PropertyIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;
import java.io.IOException;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class GspFieldIncrementDeserializer extends JsonDeserializer<GspCommonFieldIncrement> {
    @Override
    public GspCommonFieldIncrement deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(jsonParser);
        String incrementTypeStr = node.get(CefNames.IncrementType).textValue();
        if (incrementTypeStr == null || "".equals(incrementTypeStr))
            return null;
        GspCommonFieldIncrement increment = null;
        IncrementType incrementType = IncrementType.valueOf(incrementTypeStr);
        switch (incrementType) {
            case Added:
                increment = readAddIncrementInfo(node);
                break;
            case Modify:
                increment = readModifyIncrementInfo(node);
                break;
            case Deleted:
                increment = readDeletedIncrementInfo(node);
                break;
        }

        readExtendInfo(increment, node);
        return increment;
    }

    //region Add
    private AddedFieldIncrement readAddIncrementInfo(JsonNode node) {
        AddedFieldIncrement addIncrement = new AddedFieldIncrement();
        readBaseAddedInfo(addIncrement, node);
        readExtendAddedInfo(addIncrement, node);
        return addIncrement;
    }

    private void readBaseAddedInfo(AddedFieldIncrement addIncrement, JsonNode node) {
        JsonNode addVauleNode = node.get(CefNames.AddedDataType);
        if (addVauleNode == null)
            return;

        CefFieldDeserializer deserializer = getFieldDeserializer();
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(IGspCommonField.class, deserializer);
        mapper.registerModule(module);
        try {
            addIncrement.setField((GspCommonField) mapper.readValue(mapper.writeValueAsString(addVauleNode), IGspCommonField.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException("IGspCommonField反序列化失败", e);
        } catch (IOException e) {
            throw new RuntimeException("IGspCommonField反序列化失败", e);
        }
    }

    protected abstract CefFieldDeserializer getFieldDeserializer();

    protected void readExtendAddedInfo(AddedFieldIncrement value, JsonNode node) {

    }

    //endregion

    //region Modify

    private ModifyFieldIncrement readModifyIncrementInfo(JsonNode node) {
        ModifyFieldIncrement increment = new ModifyFieldIncrement();
        readBaseModifyInfo(increment, node);
        readExtendModifyInfo(increment, node);
        return increment;
    }

    private void readBaseModifyInfo(ModifyFieldIncrement increment, JsonNode node) {
        JsonNode propIncrements = node.get(CefNames.PropertyIncrements);
        if (propIncrements != null)
            readPropertyIncrementsInfo(increment, propIncrements);
    }

    private void readPropertyIncrementsInfo(ModifyFieldIncrement increment, JsonNode node) {
        ArrayNode array = trans2Array(node);
        for (JsonNode childNode : array) {
            PropertyIncrementDeserializer serializer = getPropertyIncrementSerializer();
            serializer.setCefFieldDeserializer(getFieldDeserializer());
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(PropertyIncrement.class, serializer);
            mapper.registerModule(module);
            try {
                String key = childNode.get(CefNames.Id).textValue();
                JsonNode valueNode = childNode.get(CefNames.Value);

                PropertyIncrement prop = mapper.readValue(mapper.writeValueAsString(valueNode), PropertyIncrement.class);
                increment.getChangeProperties().put(key, prop);
            } catch (JsonProcessingException e) {
                throw new RuntimeException("PropertyIncrement反序列化失败", e);
            } catch (IOException e) {
                throw new RuntimeException("PropertyIncrement反序列化失败", e);
            }
        }
    }

    private PropertyIncrementDeserializer getPropertyIncrementSerializer() {
        return new PropertyIncrementDeserializer();
    }

    protected void readExtendModifyInfo(ModifyFieldIncrement value, JsonNode node) {

    }
    //endregion

    //region Delete
    private DeletedFieldIncrement readDeletedIncrementInfo(JsonNode node) {
        JsonNode deleteIdNode = node.get(CefNames.DeletedId);
        if (deleteIdNode == null)
            return null;
        String deleteId = deleteIdNode.textValue();
        DeletedFieldIncrement increment = new DeletedFieldIncrement(deleteId);
        readExtendDeletedInfo(increment, node);
        return increment;
    }

    protected void readExtendDeletedInfo(DeletedFieldIncrement value, JsonNode node) {

    }

    //endregion

    protected void readExtendInfo(GspCommonFieldIncrement value, JsonNode node) {

    }

    private ArrayNode trans2Array(JsonNode node) {
        try {
            return new ObjectMapper().treeToValue(node, ArrayNode.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("JsonNode转化失败，检查Json结构", e);
        }
    }
}
