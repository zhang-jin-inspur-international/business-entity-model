/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.actions;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;

public abstract class JavaBaseCompCodeGenerator extends JavaBaseCommonCompCodeGen {

  protected JavaBaseCompCodeGenerator(GspBusinessEntity be,
      com.inspur.edp.bef.bizentity.operation.BizOperation operation, String nameSpace,
      String path) {
    super(be, operation, nameSpace, path);
  }

  protected String getClassNameFromComp(IMetadataContent content) {
    String fullClassName = "";
    if (content instanceof GspComponent) {
      GspComponent component = (GspComponent) content;
      fullClassName = component.getMethod().getClassName();
    } else {
      throw new RuntimeException("当前元数据不属于构件元数据");
    }
    String[] sections = fullClassName.split("[.]", -1);

    return sections[sections.length - 1];
  }

  protected void JavaGenerateExecute(StringBuilder result) {
    result.append(GetIndentationStr()).append("@").append(JavaCompCodeNames.KeywordOverride)
        .append(getNewline());
    result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ")
        .append(" ").append(JavaCompCodeNames.KeywordVoid).append(" ").append("execute()")
        .append(" ").append("{").append(getNewline());

    result.append(GetIndentationStr()).append("}");
  }

  @Override
  protected void generateBeanAnnotation(StringBuilder result) {
    return;
  }
}
