/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator;

public final class JavaCompCodeNames {

  //public static string ActionApiNameSpace = "Inspur.Gsp.Bef.Api.Action";
  public static String ActionApiNameSpace = "com.inspur.edp.bef.api.action.determination.*";
  //public static string ValidationNameSpace = "Inspur.Gsp.Bef.Spi.Action.Validation";
  public static String ValidationNameSpace = "com.inspur.edp.bef.spi.action.validation.*";
  public static String AbstractValidationNameSpace = "com.inspur.edp.bef.spi.action.validation.AbstractValidation";
  public static String AbstractActionNameSpace = "com.inspur.edp.bef.spi.action.AbstractAction";
  public static String IValidationContextNameSpace = "com.inspur.edp.bef.api.action.validation.IValidationContext";
  public static String AbstractDeterminationNameSpace = "com.inspur.edp.bef.spi.action.determination.AbstractDetermination";
  public static String IDeterminationContextNameSpace = "com.inspur.edp.bef.api.action.determination.IDeterminationContext";
  public static String IBeforeRetrieveDtmContextContextNameSpace = "com.inspur.edp.bef.api.action.determination.IBeforeRetrieveDtmContext";
  public static String IEntityDataNameSpace = "com.inspur.edp.cef.entity.entity.IEntityData";

  public static String VoidActionResultNameSpace = "com.inspur.edp.bef.api.action.VoidActionResult";
  public static String AbstractManagerActionNameSpace = "com.inspur.edp.bef.spi.action.AbstractManagerAction";
  public static String RootAbstractActionNameSpaceNameSpace = "com.inspur.edp.bef.spi.action.RootAbstractAction";
  // public static string BaseNameSpace = "Inspur.Gsp.Bef.Spi.Action";
  public static String BaseNameSpace = "com.inspur.edp.bef.api.action.validation.*";
  public static String DaterminationNameSpace = "com.inspur.edp.bef.spi.action.determination.*";
  //public static string ChangesetNameSpace = "Inspur.Gsp.Cef.Entity.Changeset";
  public static String ChangesetNameSpace = "com.inspur.edp.cef.entity.changeset.*";
  //public static string ContextNameSpace = "Inspur.Gsp.Bef.Api.BE";
  public static String ContextNameSpace = "com.inspur.edp.bef.api.be.*";
  //public static string MessageNameSpace = "Inspur.Gsp.Bef.Api.Message";
  public static String MessageNameSpace = "com.inspur.edp.cef.api.message.*";
  public static String IBusinessEntity = "IBusinessEntity";
  public static String IBEService = "IBEService";
  public static String IEntityData = "IEntityData";
  public static String BeanPackage = "org.springframework.context.annotation.Bean";
  public static String ConfigurationPackage = "org.springframework.context.annotation.Configuration";
  public static String TccHandlerContextPackage = "com.inspur.edp.bef.api.action.tcc.TccHandlerContext";
  public static String SPRING_FACTORIES_PROPERTYKEY = "org.springframework.boot.autoconfigure.EnableAutoConfiguration";

  public static String AbstractValidationClassName = "abstractValidation";

  public static String DateNameSpace = "java.util.Date";
  public static String BigDecimalNameSpace = "java.math.BigDecimal";
  public static String ArrayListNameSpace = "java.util.ArrayList";
  //public static string KeywordUsing = "using";
  public static String KeywordImport = "import";
  // public static string KeywordNameSpace = "namespace";
  public static String KeywordPackage = "package";
  public static String KeywordPublic = "public";
  public static String KeywordProtected = "protected";
  public static String KeywordInternal = "internal";
  public static String KeywordPrivate = "private";
  public static String KeywordClass = "class";
  public static String KeywordOverride = "Override";
  public static String KeywordVoid = "void";
  public static String KeywordExtends = "extends";
  public static String KeywordString = "String";
  public static String KeywordBoolean = "Boolean";
  public static String KeywordBool = "boolean";
  public static String KeywordConfiguration = "Configuration";
  public static String KeyWordBean = "@Bean(\"%s\")";
  public static String KeyWordNew = "new";
  public static String KeyWordReturn = "return";
  //public static string ActionNameSpaceSuffix = "BE";
  //public static string MgrActionNameSpaceSuffix = "BEMgr";

  public static String ActionNameSpaceSuffix = "entityactions";
  public static String MgrActionNameSpaceSuffix = "bizactions";

  public static String DeterminationNameSpaceSuffix = "Determinations";
  public static String ValidationNameSpaceSuffix = "Validations";
  public static String TccActionsNameSpaceSuffix = "tccactions";

  ///#region 构件元数据名称后缀(操作类型+Controller)
  public static final String ActionControllerName = "ActionController";
  public static final String ActionMgrControllerName = "MgrActionController";
  public static final String DeterminationControllerName = "DeterminationController";
  public static final String ValidationControllerName = "ValidationController";
  public static final String TccActionControllerName = "Controller";
  public static final String TccHandlerContextName = "ctx";

  ///#endregion

  ///#region 构件元数据扩展名
  public static final String ComponentExtensionName = ".cmp";
  public static final String BEMgrCmpExtendName = ".beMgrCmp";
  public static final String BECmpExtendName = ".beCmp";
  public static final String DtmCmpExtendName = ".beDtmCmp";
  public static final String ValCmpExtendName = ".beValCmp";
  //public static string UDTValCmpExtendName = ".udtValCmp";
  //public static string VMCmpExtendName = ".vmCmp";
  ///#endregion
  ///#region 构件元数据类型标识
  public static final String BEMgrComponent = "BEMgrComponent";
  public static final String BEComponent = "BEComponent";
  public static final String DeterminationComponent = "DeterminationComponent";
  public static final String ValidationComponent = "ValidationComponent";

  public static final String TccAction = "TccAction";
  public static final String AbstractBefTccHandler = "com.inspur.edp.bef.spi.action.tcc.AbstractBefTccHandler";

  // TCC ActionMethodName
  public static final String getNameInTcc = "getName";
  public static final String CanExecuteInTcc = "canExecute";
  public static final String ConfirmInTcc = "confirm";
  public static final String CancelInTcc = "cancel";
  public static final String CefConfigFileSuffix = "CefConfig";

  // TCC Action
  public static final String BeginCommonAnnotation = "//";
  public static final String BeginAnnotation = "/**";
  public static final String EndAnnotation = "*/";
  public static final String KeyWordParam = "@param";
  public static final String TccActionClassAnno = "为提高性能, 此类的实例创建后会被重复使用, 因此请勿在此类中添加任何成员变量保证无状态";
  public static final String TccActionCanExecuteAnno = "请在此方法中实现判断是否执行confirm或cancel的逻辑";
  public static final String CanExecuteInputParamAnno = "上包含tcc二阶段执行所需的参数, 其中ctx.getData()可获取当前数据行,ctx.getBusinessActionContext()可获取tcc上下文.";
  public static final String CanExecuteReturnParamAnno = "返回true则执行confirm或cancel, 否则跳过此tcc处理.";

  public static final String ConfirmMethodAnno = "请在此处实现tcc二阶段\"提交\"逻辑,请注意.同canExecute方法中的ctx参数";
  public static final String ConfirmParamAnno = "同canExecute方法中的ctx参数";
  public static final String CancelParamAnno = "请在此处实现tcc二阶段\"回滚\"逻辑,请注意, 按照tcc编码规范, 此处不应抛出任何异常.";

  ///#endregion
}
