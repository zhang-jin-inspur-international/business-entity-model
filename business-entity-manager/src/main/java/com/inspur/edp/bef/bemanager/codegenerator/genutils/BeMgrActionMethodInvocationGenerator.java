/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.bemgrcomponent.BizMgrActionParameter;
import com.inspur.edp.cef.designtime.core.utilsgenerator.JavaAccessModifier;
import com.inspur.edp.cef.designtime.core.utilsgenerator.JavaClassInfo;
import com.inspur.edp.cef.designtime.core.utilsgenerator.MethodInfo;
import com.inspur.edp.cef.designtime.core.utilsgenerator.TypeRefInfo;
import org.springframework.util.StringUtils;

public class BeMgrActionMethodInvocationGenerator {

  private final BizMgrAction mgrAction;
  private final JavaClassInfo classInfo;
  private final GspBusinessEntity be;
  private final MethodInfo methodInfo = new MethodInfo();

  BeMgrActionMethodInvocationGenerator(GspBusinessEntity be, BizMgrAction mgrAction,
      JavaClassInfo classInfo) {
    this.be = be;
    this.mgrAction = mgrAction;
    this.classInfo = classInfo;
  }

  private String getMethodName() {
    return mgrAction.getCode();
  }

  private TypeRefInfo getReturnType() {
    return getTypeInfo(mgrAction);
  }

  public static TypeRefInfo getTypeInfo(BizMgrAction action) {
    TypeRefInfo typeInfo = new TypeRefInfo(String.class);
    if (StringUtils.isEmpty(action.getReturnValue().getClassName()) || action.getReturnValue()
        .getClassName().toLowerCase().equalsIgnoreCase("void")) {
      typeInfo.setTypeName("void");
    } else {
      typeInfo.setTypeName(action.getReturnValue().getClassName());
    }
    return typeInfo;
  }

  public void generate() {
    methodInfo.setMethodName(getMethodName());
    methodInfo.setReturnType(getReturnType());
    methodInfo.getAccessModifiers().add(JavaAccessModifier.Public);
    methodInfo.getAccessModifiers().add(JavaAccessModifier.Static);

    if (mgrAction.getParameters() != null && mgrAction.getParameters().getCount() > 0) {
      for (Object bizParameter : mgrAction.getParameters()) {
        BizMgrActionParameter actionParameter = (BizMgrActionParameter) bizParameter;
        TypeRefInfo typeInfo = new TypeRefInfo(String.class);
        typeInfo.setTypeName(actionParameter.getClassName());
        methodInfo.getParameters().add(
            new com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo(
                actionParameter.getParamCode(), typeInfo));
      }
    }
//        methodInfo.getParameters().add(new com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo(varData,new TypeRefInfo(IEntityData.class)));
    generateMethodBodies();
    classInfo.addMethodInfo(methodInfo);
  }

  private void generateMethodBodies() {
    classInfo.getImportInfos().addImportPackage("com.inspur.edp.bef.api.be.MgrActionUtils");
    classInfo.getImportInfos().addImportPackage("com.inspur.edp.bef.api.lcp.IStandardLcp;");
    classInfo.getImportInfos().addImportPackage("com.inspur.edp.bef.api.BefRtBeanUtil;");
    methodInfo.getMethodBodies().add(
        "IStandardLcp lcp = BefRtBeanUtil.getLcpFactory().createLcp(\"" + this.be
            .getGeneratedConfigID() + "\");");
    StringBuilder sb = new StringBuilder();
    if (mgrAction.getParameters() != null && mgrAction.getParameters().getCount() > 0) {
      for (Object bizParameter : mgrAction.getParameters()) {
        BizMgrActionParameter actionParameter = (BizMgrActionParameter) bizParameter;
        sb.append("," + actionParameter.getParamCode());
      }
    }
    if (methodInfo.getReturnType().getTypeName().equalsIgnoreCase("void")) {
      methodInfo.getMethodBodies().add(
          "MgrActionUtils.executeCustomAction(lcp,\"" + mgrAction.getCode() + "\"" + sb.toString()
              + ");");
    } else {
      methodInfo.getMethodBodies().add("return (" + methodInfo.getReturnType().getTypeName()
          + ")MgrActionUtils.executeCustomAction(lcp,\"" + mgrAction.getCode() + "\"" + sb
          .toString() + ");");
    }
  }
}
