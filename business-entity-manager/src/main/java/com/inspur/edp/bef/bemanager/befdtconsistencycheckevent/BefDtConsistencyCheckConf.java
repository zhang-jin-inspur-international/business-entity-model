/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.befdtconsistencycheckevent;

import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
public
class BefDtConsistencyCheckConf {
    @Bean(name = "BefDtConsistencyCheckEventBroker")
    public BefDtConsistencyCheckEventBroker setEventBroker(EventListenerSettings settings, BefDtConsistencyCheckEventManager eventManager) {
        return new BefDtConsistencyCheckEventBroker(settings, eventManager);
    }

    @Bean(name = "BefDtConsistencyCheckEventManager")
    public BefDtConsistencyCheckEventManager setEventManager() {
        return new BefDtConsistencyCheckEventManager();
    }
}
