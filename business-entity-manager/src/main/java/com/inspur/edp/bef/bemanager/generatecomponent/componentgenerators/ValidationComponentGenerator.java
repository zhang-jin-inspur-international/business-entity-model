/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bemanager.generatecomponent.ComponentClassNameGenerator;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.VoidReturnType;
import com.inspur.edp.bef.component.detailcmpentity.validation.ValidationComponent;

public class ValidationComponentGenerator extends BaseComponentGenerator {

  public static ValidationComponentGenerator getInstance() {
    return new ValidationComponentGenerator();
  }

  private ValidationComponentGenerator() {
  }

  @Override
  protected GspComponent buildComponent() {
    return new ValidationComponent();
  }


  ///#region 将Validation中信息赋值给ValidationComponent
  @Override
  protected void evaluateComponentInfo(GspComponent component, BizOperation validation,
      GspComponent originalComponent) {
    this.originalComponent = originalComponent;
    // 基本信息（没有参数信息）
    evaluateComponentBasicInfo((ValidationComponent) component, (Validation) validation);
    // 返回值信息
    ((ValidationComponent) component).getValidationMethod().setReturnValue(new VoidReturnType());
  }

  private void evaluateComponentBasicInfo(ValidationComponent component, Validation validation) {
    if (!CheckInfoUtil.checkNull(validation.getComponentId())) {
      component.setComponentID(validation.getComponentId());
    }
    component.setComponentCode(validation.getCode());
    component.setComponentName(validation.getName());
    component.setComponentDescription(validation.getDescription());
    component.getValidationMethod().setDotnetAssembly(this.assemblyName);

    String suffix = String.format("%1$s%2$s%3$s", validation.getOwner().getCode(), '.',
        JavaCompCodeNames.ValidationNameSpaceSuffix);
    String packageName = JavaModuleImportPackage(this.nameSpace);
    packageName = String.format("%1$s%2$s", packageName, suffix.toLowerCase());

    if (this.originalComponent != null) {
      component.getValidationMethod()
          .setDotnetClassName(this.originalComponent.getMethod().getDotnetClassName());
      component.getMethod().setClassName(
          JavaModuleClassName(this.originalComponent.getMethod().getDotnetClassName(),
              packageName));
    } else {
      String classNameSuffix = this.objCode + validation.getCode();
      component.getValidationMethod().setDotnetClassName(ComponentClassNameGenerator
          .generateValidationComponentClassName(this.nameSpace, classNameSuffix));
      component.getMethod().setClassName(JavaModuleClassName(ComponentClassNameGenerator
          .generateValidationComponentClassName(this.nameSpace, classNameSuffix), packageName));
    }

  }

  ///#endregion
}
