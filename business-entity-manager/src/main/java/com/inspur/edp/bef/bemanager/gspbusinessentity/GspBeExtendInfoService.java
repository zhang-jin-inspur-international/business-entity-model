/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.gspbusinessentity;

import com.inspur.edp.bef.bemanager.gspbusinessentity.repository.GspBeExtendInfoRepository;
import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.GspBeExtendInfo;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import io.iec.edp.caf.rpc.api.support.Type;
import java.util.LinkedHashMap;
import java.util.List;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

/**
 * BE扩展信息服务实现
 *
 * @author hanll02
 */
public class GspBeExtendInfoService extends AbstractGspBeExtendInfoService {

  protected GspBeExtendInfoRepository extendInfoRepository;

  public GspBeExtendInfoService(
      GspBeExtendInfoRepository extendInfoRepository) {
    super();
    this.extendInfoRepository = extendInfoRepository;
  }

  private final String Lcm_SU = "Lcm";
  private final String BEEXTENDINFO_SERVICE = "com.inspur.edp.bef.extendinfo.server.api.IGspBeExtendInfoRpcService";

  @Override
  public GspBeExtendInfo getBeExtendInfo(String id) {
    if (configIdGspBeExtendInfo.containsKey(id)) {
      return configIdGspBeExtendInfo.get(id);
    }
    RpcClient client = (RpcClient) SpringBeanUtils.getBean(RpcClient.class);
    LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
    params.put("id", id);
    GspBeExtendInfo info = (GspBeExtendInfo) client.invoke(GspBeExtendInfo.class,
        getMethodString("getBeExtendInfo"), this.Lcm_SU, params, null);
    putConfigIdGspBeExtendInfo(id, info);
    return info;
  }

  @Override
  public GspBeExtendInfo getBeExtendInfoByConfigId(String id) {
    if (configIdGspBeExtendInfo.containsKey(id)) {
      return configIdGspBeExtendInfo.get(id);
    }
    try {
      RpcClient client = (RpcClient) SpringBeanUtils.getBean(RpcClient.class);
      LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
      params.put("configId", id);
      GspBeExtendInfo info = (GspBeExtendInfo) client.invoke(GspBeExtendInfo.class,
          getMethodString("getBeExtendInfoByConfigId"), this.Lcm_SU, params, null);
      putConfigIdGspBeExtendInfo(id, info);
      return info;
    } catch (IncorrectResultSizeDataAccessException e) {
      throw new CAFRuntimeException("", "",
          "ConfigId为" + id + "的be有多个，请检查", null, ExceptionLevel.Error);
    }
  }

  @Override
  public List<GspBeExtendInfo> getBeExtendInfos() {
    RpcClient client = (RpcClient) SpringBeanUtils.getBean(RpcClient.class);
    LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
    Type type = getBeExtendInfoListType();
    return (List) client.invoke(type,
        getMethodString("getBeExtendInfos"), this.Lcm_SU, params, null);
  }

  @Override
  public void saveGspBeExtendInfos(List<GspBeExtendInfo> infos) {
    RpcClient client = (RpcClient) SpringBeanUtils.getBean(RpcClient.class);
    LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
    params.put("infos", infos);
    client.invoke(Void.class,
        getMethodString("saveGspBeExtendInfos"), this.Lcm_SU, params, null);
  }

  @Override
  public void deleteBeExtendInfo(String id) {
    if (id == null || "".equals(id)) {
      return;
    }
    RpcClient client = (RpcClient) SpringBeanUtils.getBean(RpcClient.class);
    LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
    params.put("id", id);
    client.invoke(Void.class,
        getMethodString("deleteBeExtendInfo"), this.Lcm_SU, params, null);
    if (configIdGspBeExtendInfo == null || configIdGspBeExtendInfo.values().size() == 0) {
      return;
    }
    if (id != null) {
      configIdGspBeExtendInfo.remove(id);
    }
    String deleteConfigId = null;
    for (GspBeExtendInfo item : configIdGspBeExtendInfo.values()) {
      if (item.getConfigId().equals(id) || item.getId().equals(id)) {
        deleteConfigId = item.getConfigId();
        break;
      }
    }
    if (deleteConfigId != null) {
      configIdGspBeExtendInfo.remove(deleteConfigId);
    }
  }

  private boolean putConfigIdGspBeExtendInfo(String id, GspBeExtendInfo info) {
    if (id == null || info == null) {
      return false;
    }
    configIdGspBeExtendInfo.put(id, info);
    return true;
  }

  private String getMethodString(String methodName) {
    return this.BEEXTENDINFO_SERVICE + "." + methodName;
  }


  private Type<List> getBeExtendInfoListType() {
    return new Type<List>(List.class, GspBeExtendInfo.class);
  }
}
