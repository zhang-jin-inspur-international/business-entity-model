/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.udt.designtime.api.entity.enumtype.UseType;

/**
 属性使用方式信息

*/
public class UseTypeInfo
{
	/**
	 属性名称
	 <see cref="string"/>

	*/
	private String privatePropertyName;
	@JsonProperty("PropertyName")
	public final String getPropertyName()
	{
		return privatePropertyName;
	}
	public final void setPropertyName(String value)
	{
		privatePropertyName = value;
	}
	/**
	 属性使用方式
	 <see cref="UseType"/>

	*/
	private UseType privatePropertyUseType = UseType.AsTemplate;
	@JsonProperty("PropertyUseType")
	public final UseType getPropertyUseType()
	{
		return privatePropertyUseType;
	}
	public final void setPropertyUseType(UseType value)
	{
		privatePropertyUseType = value;
	}
	;
	/**
	 是否可以编辑
	 <see cref="bool"/>

	*/
	private boolean privateCanEdit=true;
	@JsonProperty("CanEdit")
	public final boolean getCanEdit()
	{
		return privateCanEdit;
	}
	public final void setCanEdit(boolean value)
	{
		privateCanEdit = value;
	}


}
