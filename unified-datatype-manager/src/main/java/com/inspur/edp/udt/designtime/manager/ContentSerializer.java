/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;
import com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;

import com.inspur.edp.udt.designtime.manager.services.UpdateElementService;
import java.io.IOException;

import com.inspur.edp.udt.designtime.api.utils.Context;
import com.inspur.edp.udt.designtime.api.utils.UdtThreadLocal;

/**
 * 元数据上下文序列化
 */
public class ContentSerializer implements MetadataContentSerializer {

	String serializeResult = "{\n" +
			"    \"Type\": \"SimpleDataType\",\n" +
			"    \"Content\": {}}";
	String simpleUdtType = "SimpleDataType";
	String complexUdtType = "ComplexDataType";
	String para_Type = "Type";
	String para_Content = "Content";

	/**
	 * 序列化业务对象
	 * @param iMetadataContent
	 * @return
	 */
	@Override
	public JsonNode Serialize(IMetadataContent iMetadataContent) {
		Context context = new Context();
		context.setfull(false);
		UdtThreadLocal.set(context);
		ObjectMapper mapper = new ObjectMapper();
		try {
			JsonNode result = mapper.readTree(serializeResult);
			if (iMetadataContent.getClass().isAssignableFrom(SimpleDataTypeDef.class)) {
				String jsonResult = mapper.writeValueAsString((SimpleDataTypeDef) iMetadataContent);
				((ObjectNode) result).put(para_Type, simpleUdtType);
				JsonNode sUdtJson = mapper.readTree(jsonResult);
				((ObjectNode)result).set(this.para_Content, sUdtJson);
				UdtThreadLocal.unset();
				return result;
			} else if (iMetadataContent.getClass().isAssignableFrom(ComplexDataTypeDef.class)) {
				String jsonResult = mapper.writeValueAsString((ComplexDataTypeDef) iMetadataContent);
				((ObjectNode) result).put(para_Type, complexUdtType);
				JsonNode cUdtJson = mapper.readTree(jsonResult);
				((ObjectNode)result).set(this.para_Content, cUdtJson);
				UdtThreadLocal.unset();
				return result;
			} else {
				UdtThreadLocal.unset();
				throw new RuntimeException("udt序列化器无法序列化当前类型：" + iMetadataContent.getClass().getTypeName());
			}
		} catch (IOException e) {
			UdtThreadLocal.unset();
			e.printStackTrace();
			throw new RuntimeException("udt元数据'" + ((UnifiedDataTypeDef) iMetadataContent).getName() + "'序列化报错。" + e.getMessage());
		}
	}

	/**
	 * 元数据上下文反序列化
	 * @param metaJsonNode
	 * @return
	 */
	@Override
	public IMetadataContent DeSerialize(JsonNode metaJsonNode) {
		ObjectMapper mapper = new ObjectMapper();
    UpdateElementService elementService= UpdateElementService.getInstance();
		JsonNode jsonNode = null;
		try {
			jsonNode = mapper.readTree(handleJsonString(metaJsonNode.toString()));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
  }
		String componentType = jsonNode.get(para_Type).textValue();
		String dataType = handleJsonString(jsonNode.get(para_Content).toString());
		if (complexUdtType.equals(componentType)) {
			try {
			  ComplexDataTypeDef complexDataTypeDef=mapper.readValue(dataType,ComplexDataTypeDef.class);
			  complexDataTypeDef.updateColumnsInfo();
        elementService.handleComplexUdtChildAsso(complexDataTypeDef);
				return complexDataTypeDef;
			} catch (IOException e) {
				throw new RuntimeException("反序列化多值udt异常！"+e);
			}
		}
		if (simpleUdtType.equals(componentType)) {
			try {
			  SimpleDataTypeDef simpleDataTypeDef=mapper.readValue(dataType, SimpleDataTypeDef.class);
			  simpleDataTypeDef.updateColumnsInfo();
			  elementService.handleSimpleUdtChildAsso(simpleDataTypeDef);

				return simpleDataTypeDef;
			} catch (IOException e) {
				throw new RuntimeException("反序列化单值udt异常！"+e);
			}
		}
		return null;
	}

	/**
	 * 处理JSON字符串工具类
	 * @param contentJson
	 * @return
	 */
	private static String handleJsonString(String contentJson) {
		if (!contentJson.startsWith("\"")) {
			return contentJson;
		}
		contentJson = contentJson.replace("\\r\\n", "");
		contentJson = contentJson.replace("\\\"{", "{");
		contentJson = contentJson.replace("}\\\"", "}");
		while (contentJson.startsWith("\"")) {
			contentJson = contentJson.substring(1, contentJson.length() - 1);
		}

		contentJson = contentJson.replace("\\\"", "\"");
		contentJson = contentJson.replace("\\\\", "");
		return contentJson;
	}
}
