/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager.config;

import com.inspur.edp.caf.cef.dt.spi.CommonStructureSchemaExtension;
import com.inspur.edp.udt.designtime.api.nocode.IBusinessFieldService;
import com.inspur.edp.udt.designtime.manager.commonstructure.UdtCommonStructureExtension;
import com.inspur.edp.udt.designtime.manager.repository.BusinessFieldRepository;
import com.inspur.edp.udt.designtime.manager.services.BusinessFieldService;
import io.iec.caf.data.jpa.repository.config.EnableCafJpaRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("com.inspur.edp.udt.designtime.manager.config.UdtManagerConfig")
@EntityScan({"com.inspur.edp.udt.designtime.api.nocode"})
@EnableCafJpaRepositories({"com.inspur.edp.udt.designtime.manager.repository"})
public class UdtManagerConfig {
  @Bean("com.inspur.edp.udt.designtime.manager.config.UdtManagerConfig.UdtCommonStructureExtension")
  /**
   * 获取业务字段扩展结构
   */
  public CommonStructureSchemaExtension getBEComStructureSchemaExtension()
  {
    return  new UdtCommonStructureExtension();
  }

  /**
   * 获取业务字段服务
   * @param businessFieldRepository
   * @return
   */
  @Bean
  public IBusinessFieldService getBusinessFieldService(BusinessFieldRepository businessFieldRepository) {
    return new BusinessFieldService(businessFieldRepository);
  }
}
