/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager.udtdtconsistencychecklistener;

import com.inspur.edp.bef.bizentity.bizentitydtevent.BizEntityDTEventListener;

import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.AbstractBeEntityArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.ChangingEntityCodeEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.RemovingEntityEventArgs;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.dtconsistencycheck.ConsistencyCheckEventMessage;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.element.ElementCollection;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.List;

public class UdtDtConsistencyCheckListener extends BizEntityDTEventListener {


  /**
   * 修改节点编号检查当前BE节点的UDT关联和依赖信息
   *
   * @param args
   * @return
   */
  @Override
  public RemovingEntityEventArgs removingEntity(RemovingEntityEventArgs args) {
    return (RemovingEntityEventArgs) udtConsistencyCheck(args);
  }

  /**
   * 对依赖信息进行检查
   *
   * @param args
   * @return
   */
  protected AbstractBeEntityArgs udtConsistencyCheck(AbstractBeEntityArgs args) {
    String returnMessage = getDependencyInfos(args.getMetadataPath(), args.getBeId(),args.getBeEntityId());
    if (returnMessage == null || returnMessage.length() == 0) {
      return args;
    }
    ConsistencyCheckEventMessage message = new ConsistencyCheckEventMessage(false, returnMessage);
    args.addEventMessage(message);
    return args;
  }

  /**
   * 获取的关联信息
   *
   * @param metadataPath 元数据路径
   * @param beId         元数据ID
   * @return 关联当前BE的关联信息
   */
  protected String getDependencyInfos(String metadataPath, String beId,String beEntityId) {
    MetadataService metadataService = SpringBeanUtils
        .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataService.class);
    List<GspMetadata> gspMetadataList = metadataService
        .getMetadataListByRefedMetadataId(metadataPath, beId);
    StringBuilder strBuilder = new StringBuilder();
    gspMetadataList.forEach(gspMetadata -> {
      if (!gspMetadata.getHeader().getType().equals("UnifiedDataType")) {
        return;
      }
      UnifiedDataTypeDef unifiedDataType = (UnifiedDataTypeDef) metadataService
          .loadMetadata(gspMetadata.getHeader().getFileName(), gspMetadata.getRelativePath())
          .getContent();
      if(!getSingleUdtDependence(unifiedDataType,beEntityId) && !getMultiUdtDependence(unifiedDataType,beEntityId))
        return;
      String packageName = projectName(gspMetadata.getRelativePath());
      strBuilder.append("工程【").append(packageName)
          .append("】中UDT【").append(unifiedDataType.getCode()).append("】依赖了当前BE字段。\n");
    });
    if (strBuilder.toString() == null || strBuilder.toString().length() == 0) {
      return null;
    }
    return strBuilder.toString();
  }

  /**
   * 获取工程名称
   *
   * @param metadataPath 元数据路径
   * @return 元数据包名
   */
  protected String projectName(String metadataPath) {
    MetadataProjectService projectService = SpringBeanUtils
        .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataProjectService.class);
    return projectService.getMetadataProjInfo(metadataPath).getName();
  }

  protected boolean getSingleUdtDependence(UnifiedDataTypeDef udtElement, String beEntityId){
    if(udtElement instanceof SimpleDataTypeDef){
      GspAssociationCollection gspAssociations = ((SimpleDataTypeDef) udtElement).getChildAssociations();
      if(gspAssociations.size() == 0){
        return false;
      }
      if(gspAssociations.get(0).getRefObjectID().equals(beEntityId))
        return true;
    }
    return false;
  }

  protected boolean getMultiUdtDependence(UnifiedDataTypeDef udtElement, String beEntityId) {
    if (!(udtElement instanceof ComplexDataTypeDef))
      return false;
    ElementCollection fields = ((ComplexDataTypeDef) udtElement).getElements();
    if (fields.size() == 0)
      return false;
    for (IGspCommonField field : fields) {
      //遍历多值UDT的每个字段
      if (field.getChildAssociations().size() == 0)
        continue;
      if(field.getChildAssociations().get(0) == null)
        continue;
      if(field.getChildAssociations().get(0).getRefObjectID().equals(beEntityId))
        return true;
    }
    return false;
  }
}
