/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.gsp.commonmodel.api.typeinfos;

/**
 * 类信息
 *
 * @author haoxiaofei
 */
public final class MediateType implements Cloneable {
	public MediateType(String nameSpace, String className) {
		setClassName(className);
		setNamespace(nameSpace);
	}

	/**
	 * 命名空间
	 */
	private String privateNamespace;

	public String getNamespace() {
		return privateNamespace;
	}

	public void setNamespace(String value) {
		privateNamespace = value;
	}

	/**
	 * 类名称
	 */
	private String privateClassName;

	public String getClassName() {
		return privateClassName;
	}

	public void setClassName(String value) {
		privateClassName = value;
	}

	/**
	 * 获取类的全称
	 *
	 * @return 类全称
	 */
	public String getFullName() {
		return String.format("%1$s.%2$s", getNamespace(), getClassName());
	}

	public MediateType clone() {
		try {
			return (MediateType) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
}
