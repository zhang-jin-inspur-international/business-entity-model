/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.i18n.merge;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.entity.increment.merger.DataTypeIncrementMerger;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.CefFieldResourceExtractor;
import com.inspur.edp.cef.designtime.api.i18n.merger.CefFieldResourceMerger;
import com.inspur.edp.cef.designtime.api.i18n.merger.DataTypeResourceMerger;
import com.inspur.edp.cef.designtime.api.increment.merger.MergeUtils;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.object.GspUniqueConstraint;
import com.inspur.edp.das.commonmodel.i18n.CommonObjectResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.names.CmResourceKeyNames;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;

public abstract  class CommonobjectResourceMerger extends DataTypeResourceMerger {
    private IGspCommonObject commonObj;
    protected CommonobjectResourceMerger(IGspCommonObject commonDataType, ICefResourceMergeContext context) {
        super(commonDataType, context);
        this.commonObj= commonDataType;
    }

    protected final CefFieldResourceMerger getCefFieldResourceMerger(
            ICefResourceMergeContext context,
            IGspCommonField field) {
        return getCommonEleResourceMerger(context,
                (IGspCommonElement) ((field instanceof IGspCommonElement) ? field : null));
    }
    @Override
    protected final void extractExtendProperties(IGspCommonDataType dataType) {
        // 子节点
        if (commonObj.getContainChildObjects() != null && commonObj.getContainChildObjects().size() > 0) {
            for (IGspCommonObject childObj : commonObj.getContainChildObjects()) {
                getObjectResourceMerger(getContext(), childObj).merge();
            }
        }
        // 唯一性约束
        extractUniqueConstraints(commonObj);
        // 扩展
        extractExtendObjProperties((IGspCommonObject) ((dataType instanceof IGspCommonObject) ? dataType : null));
    }
    // #region 私有方法
    private void extractUniqueConstraints(IGspCommonObject obj) {
        if (obj.getContainConstraints() != null && obj.getContainConstraints().size() > 0) {
            for (GspUniqueConstraint con : obj.getContainConstraints()) {
                extractUniqueConstraint(con);
            }
        }
    }

    private void extractUniqueConstraint(GspUniqueConstraint item) {
        if (item.getConstraintMessage() == null || "".equals(item.getConstraintMessage())) {
            return;
        }
        I18nResourceItemCollection resourceItems=getContext().getResourceItems();
        String keyPrefix= MergeUtils.getKeyPrefix(item.getI18nResourceInfoPrefix(),CmResourceKeyNames.TipInfo);
        item.setName(resourceItems.getResourceItemByKey(keyPrefix).getValue());
    }

    // #endregion

    protected abstract void extractExtendObjProperties(IGspCommonObject dataType);

    protected abstract CommonElementResourceMerger getCommonEleResourceMerger(
            ICefResourceMergeContext context,
            IGspCommonElement field);

    protected abstract CommonobjectResourceMerger getObjectResourceMerger(
            ICefResourceMergeContext context,
            IGspCommonObject obj);
}
