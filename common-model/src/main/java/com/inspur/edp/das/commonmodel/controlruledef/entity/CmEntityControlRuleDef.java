/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.controlruledef.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmEntityControlRuleDefParser;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmEntityControlRuleDefSerializer;

@JsonSerialize(using = CmEntityControlRuleDefSerializer.class)
@JsonDeserialize(using = CmEntityControlRuleDefParser.class)
public class CmEntityControlRuleDef extends CommonDataTypeControlRuleDef {
    public CmEntityControlRuleDef(ControlRuleDefinition parentRuleDefinition) {
        super(parentRuleDefinition, CmEntityRuleNames.CmEntityRuleObjectType);
    }

    public ControlRuleDefItem getAddChildEntityControlRule() {
        return super.getControlRuleItem(CmEntityRuleNames.AddChildEntity);
    }

    public void setAddChildEntityControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CmEntityRuleNames.AddChildEntity, ruleItem);
    }

    public ControlRuleDefItem getModifyChildEntitiesControlRule() {
        return super.getControlRuleItem(CmEntityRuleNames.ModifyChildEntities);
    }

    public void setModifyChildEntitiesControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CmEntityRuleNames.ModifyChildEntities, ruleItem);
    }
}
