/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.json.increment;

import com.inspur.edp.cef.designtime.api.element.increment.json.GspFieldIncrementSerializer;
import com.inspur.edp.cef.designtime.api.entity.increment.json.GspDataTypeIncrementSerializer;
import com.inspur.edp.cef.designtime.api.json.object.GspCommonDataTypeSerializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectSerializer;

public abstract class CommonObjectIncrementSerializer extends GspDataTypeIncrementSerializer {

    @Override
    protected final GspCommonDataTypeSerializer getDataTypeSerializer() {
        return getCommonObjectSerializer();
    }

    protected abstract CmObjectSerializer getCommonObjectSerializer();

    @Override
    protected GspFieldIncrementSerializer getFieldSerializer() {
        return getCmElementIncrementSerizlizer();
    }

    protected abstract CommonElementIncrementSerializer getCmElementIncrementSerizlizer();

    @Override
    protected final GspDataTypeIncrementSerializer getGspDataTypeIncrementSerializer() {
        return getCmObjectIncrementSerializer();
    }

    protected abstract CommonObjectIncrementSerializer getCmObjectIncrementSerializer();
}
