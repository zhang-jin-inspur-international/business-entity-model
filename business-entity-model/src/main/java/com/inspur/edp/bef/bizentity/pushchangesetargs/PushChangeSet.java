/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.pushchangesetargs;

import java.util.List;

public class PushChangeSet {

  private MetadataInfo metadataInfo;
  private List<RelatedMetadata> relatedMetadatas;


  private List<ActionChangeSet> actionChangeSets;
  private List<ObjectChangeSet> objectChangeSets;
  private List<ElementChangeSet> elementChangeSets;

  public PushChangeSet() {
  }

  public List<ActionChangeSet> getActionChangeSets() {
    return actionChangeSets;
  }

  public void setActionChangeSets(List<ActionChangeSet> actionChangeSets) {
    this.actionChangeSets = actionChangeSets;
  }

  public List<ObjectChangeSet> getObjectChangeSets() {
    return objectChangeSets;
  }

  public void setObjectChangeSets(List<ObjectChangeSet> objectChangeSets) {
    this.objectChangeSets = objectChangeSets;
  }


  public MetadataInfo getMetadataInfo() {
    return metadataInfo;
  }

  public void setMetadataInfo(MetadataInfo metadataInfo) {
    this.metadataInfo = metadataInfo;
  }

  public void setRelatedMetadatas(List<RelatedMetadata> relatedMetadatas) {
    this.relatedMetadatas = relatedMetadatas;
  }

  public List<RelatedMetadata> getRelatedMetadatas() {
    return relatedMetadatas;
  }


  public List<ElementChangeSet> getElementChangeSets() {
    return elementChangeSets;
  }

  public void setElementChangeSets(List<ElementChangeSet> elementChangeSets) {
    this.elementChangeSets = elementChangeSets;
  }

}
