/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation.collection;

import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.BizOperationCollection;

public class BizMgrActionCollection extends BizOperationCollection {

  private static final long serialVersionUID = 1L;

  public final boolean add(BizOperation operation) {
    if (operation instanceof BizMgrAction) {
      return super.add(operation);
    }
    throw new RuntimeException("错误的Action类型");
  }

  @Override
  public BizMgrActionCollection clone() {
    return (BizMgrActionCollection) super.clone();
  }

  @Override
  protected BizMgrAction convertOperation(BizOperation op) {
    return (BizMgrAction) super.convertOperation(op);
  }

  @Override
  protected BizMgrActionCollection createOperationCollection() {
    return new BizMgrActionCollection();
  }
}
