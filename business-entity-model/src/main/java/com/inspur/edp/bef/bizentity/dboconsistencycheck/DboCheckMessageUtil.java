/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.dboconsistencycheck;

/**
 * DBO检查提示信息工具类
 *
 * @author haoxiaofei
 */
public class DboCheckMessageUtil {

  static String DBOInfo = "数据对象ID为[%s];";
  static String BEInfo = "实体名称为:[%s],实体编号为:[%s];";
  static String ObjectInfo = "实体节点名称[%s],节点编号为:[%s];";
  static String FieldInfo = "实体字段名称[%s],字段编号为:[%s];";
  static String DBONotFound = "当前节点找不到对应的数据库对象,";
  static String ColumnNotFound = "BE字段找不到对应数据库对象的列，";
  static String FieldMDataTypeNotMatch = "字段数据类型不一致,BE字段数据类型为[%s],DBO列数据类型为[%s],";
  static String AssoRefMetadataNotFound = "关联字段找不到依赖的业务实体,实体ID为[%s],";
  static String AssoRefObjectNotFound = "关联字段找不到依赖的实体节点,实体ID为[%s],实体节点ID为[%s],";
  static String AssoRefFieldNotFound = "关联带出字段[%s]找不到依赖的实体字段，元数据ID为[%s],实体节点ID为[%s]，实体字段ID为[%s]";
  static String NewLine = System.lineSeparator();
  static String exceptionCode = "GenerateBeCheck";
  static String FieldLengthExp = "BE字段长度大于对应DBO长度，BE字段长度为[%s],DBO列长度为[%s],";
  static String FieldPrecisionExp = "BE字段精度大于对应DBO精度，BE字段精度为[%s],DBO列精度为[%s],";
}
