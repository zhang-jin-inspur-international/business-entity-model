/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.TccSettingElement;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

public class TccSettingElementDeserializer extends JsonDeserializer<TccSettingElement> {

  @Override
  public final TccSettingElement deserialize(JsonParser jsonParser,
      DeserializationContext context) {
    return deserializeTccSettingElement(jsonParser);
  }


  private TccSettingElement deserializeTccSettingElement(JsonParser jsonParser) {
    TccSettingElement tccElement = createTccSettingElement();

    SerializerUtils.readStartObject(jsonParser);
    while (jsonParser.getCurrentToken() == FIELD_NAME) {
      String propName = SerializerUtils.readPropertyName(jsonParser);
      readPropertyValue(tccElement, propName, jsonParser);
    }
    SerializerUtils.readEndObject(jsonParser);
    if (tccElement.getTccAction().getCode() == null) {
      tccElement.getTccAction().setCode(tccElement.getCode());
    }
    if (tccElement.getTccAction().getName() == null) {
      tccElement.getTccAction().setName(tccElement.getName());
    }
    return tccElement;
  }

  private void readPropertyValue(TccSettingElement ele, String propName, JsonParser jsonParser) {
    switch (propName) {
      case CommonModelNames.ID:
        ele.setID(SerializerUtils.readPropertyValue_String(jsonParser));
        break;
      case CommonModelNames.Code:
        ele.setCode(SerializerUtils.readPropertyValue_String(jsonParser));
        break;
      case CommonModelNames.Name:
        ele.setName(SerializerUtils.readPropertyValue_String(jsonParser));
        break;
      case BizEntityJsonConst.TriggerFields:
        ele.setTriggerFields(SerializerUtils.readStringArray(jsonParser));
        break;
      case BizEntityJsonConst.TccAction:
        TccActionDeserializer deserializer = new TccActionDeserializer();
        ele.setTccAction(deserializer.deserialize(jsonParser, null));
        break;
      default:
        throw new RuntimeException("未定义的数据类型" + propName);
    }
  }

  protected TccSettingElement createTccSettingElement() {
    return new TccSettingElement();
  }
}
