/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.i18n;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.i18n.CommonElementResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.CommonObjectResourceExtractor;

public class BizObjResourceExtractor extends CommonObjectResourceExtractor {

  public BizObjResourceExtractor(GspBizEntityObject commonDataType,
      ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
    super(commonDataType, context, parentResourceInfo);
  }

  @Override
  protected void extractExtendObjProperties(IGspCommonObject dataType) {
  }

  @Override
  protected CommonElementResourceExtractor getCommonEleResourceExtractor(
      ICefResourceExtractContext context, CefResourcePrefixInfo objPrefixInfo,
      IGspCommonElement field) {
    return new BizEleResourceExtractor(
        (GspBizEntityElement) ((field instanceof GspBizEntityElement) ? field : null), context,
        objPrefixInfo);
  }

  @Override
  protected CommonObjectResourceExtractor getObjectResourceExtractor(
      ICefResourceExtractContext context, CefResourcePrefixInfo modelPrefixInfo,
      IGspCommonObject obj) {
    return new BizObjResourceExtractor(
        (GspBizEntityObject) ((obj instanceof GspBizEntityObject) ? obj : null), context,
        modelPrefixInfo);
  }
}
