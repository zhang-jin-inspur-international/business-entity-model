/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation.componentbase;

import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameterCollection;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * 构件参数集合类
 */
public class BizParameterCollection<T extends IBizParameter> extends ArrayList<T> implements
    IBizParameterCollection<T> {

  private java.util.ArrayList<T> compParameters = new java.util.ArrayList<T>();

  public BizParameterCollection() {
  }

  // region IList成员
  public final T getItem(int index) {
    if (index > -1 && index < getCount()) {
      return compParameters.get(index);
    }
    return null;
  }

  public final void setItem(int index, T value) {
    compParameters.set(index, value);
  }

  /**
   * 新增参数
   *
   * @param item
   */
  public final boolean add(IBizParameter item) {
    return compParameters.add((T) item);
  }

  public final void insert(int index, IBizParameter item) {
    compParameters.add(index, (T) item);
  }

  public final void removeAt(int index) {
    compParameters.remove(index);
  }

  /**
   * 移除指定的参数
   */
  public final boolean remove(IBizParameter item) {
    boolean flag = false;
    for (int i = 0; i < compParameters.size(); i++) {
      if (compParameters.get(i).equals(item)) {
        compParameters.remove(i);
        flag = true;
        break;
      }
    }
    return flag;
  }

  /**
   * 是否包含指定参数
   *
   * @param item
   * @return
   */
  public final boolean contains(IBizParameter item) {
    return compParameters.contains(item);
  }

  /**
   * 清除所有内容
   */
  public final void clear() {
    compParameters.clear();
  }

  /**
   * 返回指定项目的索引
   *
   * @return 位置
   */
  public final int indexOf(IBizParameter item) {
    for (int i = 0; i < compParameters.size(); i++) {
      if (compParameters.get(i).equals(item)) {
        return i;
      }
    }
    return -1;
  }

  // endregion

  // region ICollection 成员

  public final int getCount() {
    return compParameters.size();
  }

  // endregion

  @Override
  public Iterator<T> iterator() {
    return compParameters.iterator();
  }
}
