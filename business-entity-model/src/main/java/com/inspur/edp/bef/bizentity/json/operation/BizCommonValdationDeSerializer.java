/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizCommonValdation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonValDeserializer;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;

public class BizCommonValdationDeSerializer extends CommonValDeserializer {

  protected CommonOperation CreateCommonOp() {
    return new BizCommonValdation();
  }

  @Override
  protected boolean readExtendValProp(CommonOperation op, String propName, JsonParser jsonParser) {
    boolean result = false;
    CommonValidation val = (CommonValidation) op;
    switch (propName) {
      case BizEntityJsonConst.Parameters:
        result = true;
        readParameters(jsonParser, (BizCommonValdation) val);
        break;
    }
    return result;
  }

  private void readParameters(JsonParser jsonParser, BizCommonValdation action) {
    BizParaDeserializer paraDeserializer = new BizActionParaDeserializer();
    BizParameterCollection<BizParameter> collection = new BizParameterCollection<>();
    SerializerUtils.readArray(jsonParser, paraDeserializer, collection);
    action.setParameterCollection(collection);
  }
}
