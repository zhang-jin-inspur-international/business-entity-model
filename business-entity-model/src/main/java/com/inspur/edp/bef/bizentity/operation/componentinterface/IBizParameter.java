/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation.componentinterface;

import com.inspur.edp.bef.bizentity.operation.componentbase.BizParActualValue;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizCollectionParameterType;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterMode;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterType;

public interface IBizParameter {

  String getID();

  void setID(String value);

  String getParamCode();

  void setParamCode(String value);

  String getParamName();

  void setParamName(String value);

  BizParameterType getParameterType();

  void setParameterType(BizParameterType value);

  BizCollectionParameterType getCollectionParameterType();

  void setCollectionParameterType(BizCollectionParameterType value);

  String getAssembly();

  void setAssembly(String value);

  String getClassName();

  void setClassName(String value);

  BizParameterMode getMode();

  void setMode(BizParameterMode value);

  String getParamDescription();

  void setParamDescription(String value);

  /**
   * @return The Actual Value  Of The Parameter,It`s Not Required
   */
  BizParActualValue getActualValue();

  /**
   * @param value The Actual Value  Of The Parameter,It`s Not Required
   */
  void setActualValue(BizParActualValue value);
}
