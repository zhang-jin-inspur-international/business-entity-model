/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.object;


import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.common.InternalActionUtil;
import com.inspur.edp.bef.bizentity.json.element.BizElementSerializer;
import com.inspur.edp.bef.bizentity.json.operation.BizActionCollectionSeriallizer;
import com.inspur.edp.bef.bizentity.json.operation.BizCommonDeterminationSerializer;
import com.inspur.edp.bef.bizentity.json.operation.BizCommonValidationSerializer;
import com.inspur.edp.bef.bizentity.json.operation.DtmCollectionSerializer;
import com.inspur.edp.bef.bizentity.json.operation.TccSettingCollectionSerializer;
import com.inspur.edp.bef.bizentity.json.operation.ValCollectionSerializer;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.collection.BizActionCollection;
import com.inspur.edp.bef.bizentity.operation.collection.DeterminationCollection;
import com.inspur.edp.bef.bizentity.operation.collection.TccSettingCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonDtmSerializer;
import com.inspur.edp.cef.designtime.api.json.operation.CommonValSerializer;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.json.element.CmElementSerializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectSerializer;
import java.io.IOException;
import lombok.SneakyThrows;
import lombok.var;

public class BizObjectSerializer extends CmObjectSerializer {

  public BizObjectSerializer() {
    dtmCollectionSerializer = new DtmCollectionSerializer(isFull);
    valCollectionSerializer = new ValCollectionSerializer(isFull);
    actionCollectionSeriallizer = new BizActionCollectionSeriallizer(isFull);
    tccSettingCollectionSerializer = new TccSettingCollectionSerializer(isFull);
  }

  public BizObjectSerializer(boolean full) {
    super(full);
    isFull = full;
    dtmCollectionSerializer = new DtmCollectionSerializer(isFull);
    valCollectionSerializer = new ValCollectionSerializer(isFull);
    actionCollectionSeriallizer = new BizActionCollectionSeriallizer(isFull);
    tccSettingCollectionSerializer = new TccSettingCollectionSerializer(isFull);
  }

  private DtmCollectionSerializer dtmCollectionSerializer;
  private ValCollectionSerializer valCollectionSerializer;
  private BizActionCollectionSeriallizer actionCollectionSeriallizer;
  private TccSettingCollectionSerializer tccSettingCollectionSerializer;

  //region BaseProp
  @Override
  protected void writeExtendObjectBaseProperty(JsonGenerator writer,
      IGspCommonObject commonObject) {
    GspBizEntityObject bizObject = (GspBizEntityObject) commonObject;
    writeDeterminations(writer, bizObject);
    writeValidations(writer, bizObject);
    WriteBizActions(writer, bizObject);
    WriteTccSettings(writer, bizObject);

    if (isFull || (bizObject.getLogicDeleteControlInfo() != null && bizObject
        .getLogicDeleteControlInfo().getEnableLogicDelete())) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.LogicDeleteControlInfo);
      SerializerUtils.writePropertyValue_Object(writer, bizObject.getLogicDeleteControlInfo());
    }

//        WriteAuthFieldInfos(writer, bizObject);
  }

  @Override
  public CommonDtmSerializer getCommonDtmSerializer(boolean isFull) {
    return new BizCommonDeterminationSerializer(isFull);
  }

  @Override
  protected CommonValSerializer getCommonValSerializer() {
    return new BizCommonValidationSerializer(this.isFull);
  }

  private void writeDeterminations(JsonGenerator writer, GspBizEntityObject bizObject) {
    if (isFull || (bizObject.getDeterminations() != null
        && bizObject.getDeterminations().size() > 0)) {
      //元数据打开 或者旧版设计器保存的时候，需要序列化
      if ("V1".equals(getFlag())) {
        writeDtmList(writer, BizEntityJsonConst.Determinations, bizObject.getDeterminations());
      }
    }
    if (isFull || (bizObject.getItemDeletingDtms() != null && !bizObject.getItemDeletingDtms()
        .isEmpty())) {
      writeDtm(BizEntityJsonConst.ItemDeletingDtm, bizObject.getItemDeletingDtms(), writer);
    }
    if (isFull || (bizObject.getDtmBeforeQuery() != null && !bizObject.getDtmBeforeQuery()
        .isEmpty())) {
      writeDtm(BizEntityJsonConst.B4QueryDtm, bizObject.getDtmBeforeQuery(), writer);
    }
    if (isFull || (bizObject.getDtmAfterQuery() != null && !bizObject.getDtmAfterQuery()
        .isEmpty())) {
      writeDtm(BizEntityJsonConst.AftQueryDtm, bizObject.getDtmAfterQuery(), writer);
    }
    if (isFull || (bizObject.getDtmBeforeRetrieve() != null && !bizObject.getDtmBeforeRetrieve()
        .isEmpty())) {
      writeDtm(BizEntityJsonConst.B4RetrieveDtm, bizObject.getDtmBeforeRetrieve(), writer);
    }
    if (isFull || (bizObject.getDtmAfterLoading() != null && !bizObject.getDtmAfterLoading()
        .isEmpty())) {
      writeDtm(BizEntityJsonConst.AftLoadingDtm, bizObject.getDtmAfterLoading(), writer);
    }
    if (isFull || (bizObject.getDtmCancel() != null && !bizObject.getDtmCancel().isEmpty())) {
      writeDtm(BizEntityJsonConst.DtmCancel, bizObject.getDtmCancel(), writer);
    }
  }

  @SneakyThrows
  private void writeDtmList(JsonGenerator writer, String name, DeterminationCollection dtms) {
    if (dtms == null || dtms.isEmpty()) {
      return;
    }
    SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Determinations);
    dtmCollectionSerializer.serialize(dtms, writer, null);
  }

  private void writeValidations(JsonGenerator writer, GspBizEntityObject bizObject) {
    if (isFull || (bizObject.getValidations() != null && bizObject.getValidations().size() > 0)) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Validations);
      var actionList = bizObject.getValidations();
      try {
        valCollectionSerializer.serialize(actionList, writer, null);
      } catch (IOException e) {
        throw new RuntimeException(bizObject.getCode() + "的校验规则序列化失败", e);
      }
    }
    if (isFull || (bizObject.getItemDeletingVals() != null && !bizObject.getItemDeletingVals()
        .isEmpty())) {
      writeVal(BizEntityJsonConst.ItemDeletingVal, bizObject.getItemDeletingVals(), writer);
    }
    if (isFull || (bizObject.getValAfterSave() != null && !bizObject.getValAfterSave().isEmpty())) {
      writeVal(BizEntityJsonConst.ValidationAfterSave, bizObject.getValAfterSave(), writer);
    }
  }

  private void WriteBizActions(JsonGenerator writer, GspBizEntityObject bizObject) {
    if (!isFull && bizObject.getBizActions() == null || bizObject.getBizActions().size() < 0) {
      return;
    }
    boolean isHaveSelfAction = false;
    BizActionCollection actionL = bizObject.getBizActions();
    for (BizOperation a : actionL) {
      if (!InternalActionUtil.InternalMgrActionIDs.contains(a.getID())
          && !InternalActionUtil.InternalBeActionIDs.contains(a.getID())) {
        isHaveSelfAction = true;
        break;
      }
    }
    if (isFull || isHaveSelfAction) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.BizActions);
      var actionList = bizObject.getBizActions();
      try {
        actionCollectionSeriallizer.serialize(actionList, writer, null);
      } catch (IOException e) {
        throw new RuntimeException(bizObject.getCode() + "的内部方法序列化失败", e);
      }
    }
  }

  private void WriteTccSettings(JsonGenerator writer, GspBizEntityObject bizObject) {
    if (isFull || (bizObject.getTccSettings() != null && bizObject.getTccSettings().size() > 0)) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.TccSettings);
      TccSettingCollection tccSettings = bizObject.getTccSettings();
      try {
        tccSettingCollectionSerializer.serialize(tccSettings, writer, null);
      } catch (IOException e) {
        throw new RuntimeException(bizObject.getCode() + "的Tcc配置字段序列化失败", e);
      }
    }
  }

//    private void WriteAuthFieldInfos(JsonGenerator writer, GspBizEntityObject bizObject)
//    {
//        AuthFieldConvertor convertor = new AuthFieldConvertor();
//        //权限字段集合：
//        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.AuthFieldInfos);
//        //[
//        SerializerUtils.WriteStartArray(writer);
//        if (bizObject.AuthFieldInfos.Count > 0)
//        {
//            bizObject.AuthFieldInfos.ForEach(
//                    item =>
//                    {
//                            convertor.WriteJson(writer, item, null);
//					});
//        }
//        //]
//        SerializerUtils.WriteEndArray(writer);
//    }

  //endregion


  @Override
  protected void writeExtendObjectSelfProperty(JsonGenerator jsonGenerator,
      IGspCommonObject iGspCommonObject) {

  }

  @Override
  protected CmElementSerializer gspCommonDataTypeSerializer() {
    return new BizElementSerializer(isFull);
  }


}



