/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.util;

import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.databaseobject.api.entity.DataType;
import java.util.Arrays;
import java.util.List;

public class DboTransferUtil {

  public static GspElementDataType getMDataTypeByDataType(DataType dataType, String exceptionCode) {
    // 列数据类型转换为字段数据类型
    switch (dataType) {
      case Int:
      case SmallInt:
        return GspElementDataType.Integer;
      case Decimal:
        return GspElementDataType.Decimal;
      case NVarchar:
      case NChar:
      case Varchar:
      case Char:
        return GspElementDataType.String;
      case Clob:
      case NClob:
        return GspElementDataType.Text;
      case DateTime:
        return GspElementDataType.Date;
      case TimeStamp:
        return GspElementDataType.DateTime;
      case Blob:
        return GspElementDataType.Binary;
      case Boolean:
        return GspElementDataType.Boolean;
      default:
        throw new BefExceptionBase("", exceptionCode,
            String.format("未定义的数据库对象字段类型'%1$s'，无法转换为BE字段类型。", dataType), null, ExceptionLevel.Error,
            false);
    }
  }

  public static List<GspElementDataType> checkMDataTypeByDataType(DataType dataType,
      String exceptionCode) {
    // 列数据类型转换为字段数据类型
    switch (dataType) {
      case Int:
      case SmallInt:
        return Arrays.asList(GspElementDataType.Integer);
      case Decimal:
        return Arrays.asList(GspElementDataType.Decimal);
      case NVarchar:
      case NChar:
      case Varchar:
        return Arrays.asList(GspElementDataType.String);
      case Char:
        return Arrays.asList(GspElementDataType.Boolean, GspElementDataType.String);
      case Clob:
      case NClob:
        return Arrays.asList(GspElementDataType.Text);
      case DateTime:
      case TimeStamp:
        return Arrays.asList(GspElementDataType.Date, GspElementDataType.DateTime);
      case Blob:
        return Arrays.asList(GspElementDataType.Binary);
      case Boolean:
        return Arrays.asList(GspElementDataType.Boolean);
      default:
        throw new BefExceptionBase("", exceptionCode,
            String.format("未定义的数据库对象字段类型'%1$s'，无法转换为BE字段类型。", dataType), null, ExceptionLevel.Error,
            false);

    }
  }
}
