/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.pushchangesetargs;

public class RelatedMetadata {

  private String contentCode;
  private String nameSpace;
  private String contentName;
  private String contentType;

  private String displayName;

  public RelatedMetadata() {
  }

  public RelatedMetadata(String nameSpace, String contentCode, String contentName,
      String contentType) {
    this.nameSpace = nameSpace;
    this.contentCode = contentCode;
    this.contentName = contentName;
    this.contentType = contentType;
  }


  public String getNameSpace() {
    return nameSpace;
  }

  public String getContentCode() {
    return contentCode;
  }

  public String getContentName() {
    return contentName;
  }

  public String getContentType() {
    return contentType;
  }

  public String getDisplayName() {
    return displayName;
  }
}
