/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.element;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.common.BizEntityEnumConst;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.json.operation.BizCommonDeterminationSerializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonDtmSerializer;
import com.inspur.edp.cef.designtime.api.json.operation.CommonValSerializer;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.json.element.CmElementSerializer;

public class BizElementSerializer extends CmElementSerializer {

  private CommonDtmSerializer dtmSerializer;
  private CommonValSerializer valSerializer;

  @Override
  protected void writeExtendElementBaseProperty(JsonGenerator jsonGenerator,
      IGspCommonElement iGspCommonElement) {

  }

  public BizElementSerializer() {
    dtmSerializer = new CommonDtmSerializer(isFull);
    valSerializer = new CommonValSerializer(isFull);
  }

  public BizElementSerializer(boolean full) {
    super(full);
    isFull = full;
    dtmSerializer = new BizCommonDeterminationSerializer(isFull);
    valSerializer = new CommonValSerializer(isFull);
  }

  @Override
  protected void writeExtendElementSelfProperty(JsonGenerator writer,
      IGspCommonElement commonElement) {
    GspBizEntityElement bizElement = (GspBizEntityElement) commonElement;
    if (isFull) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.CalculationExpress,
          bizElement.getCalculationExpress());
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ValidationExpress,
          bizElement.getValidationExpress());
    }
    if (isFull || (bizElement.getRtElementConfigId() != null && !""
        .equals(bizElement.getRtElementConfigId()))) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.RtElementConfigId,
          bizElement.getRtElementConfigId());
    }
    if (isFull || bizElement.getIsDefaultNull()) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsDefaultNull,
          bizElement.getIsDefaultNull());
    }
//        writeUnifiedDataTypeDef(writer, bizElement);
    if (isFull) {
      writeReqireCheckOccasion(writer, bizElement);
    }
    if (isFull || (bizElement.getValueChangedDtms() != null && !bizElement.getValueChangedDtms()
        .isEmpty())) {
      SerializerUtils
          .writeDtm(BizEntityJsonConst.ValueChangedDtm, writer, bizElement.getValueChangedDtms(),
              dtmSerializer);
    }
    if (isFull || (bizElement.getValueChangedVals() != null && !bizElement.getValueChangedVals()
        .isEmpty())) {
      SerializerUtils
          .writeVal(BizEntityJsonConst.ValueChangedVal, writer, bizElement.getValueChangedVals(),
              valSerializer);
    }
    if (isFull || (bizElement.getComputationDtms() != null && !bizElement.getComputationDtms()
        .isEmpty())) {
      SerializerUtils
          .writeDtm(BizEntityJsonConst.ComputationDtm, writer, bizElement.getComputationDtms(),
              dtmSerializer);
    }
  }


  //TODO 需要统一加，设计器用
  private void writeUnifiedDataTypeDef(JsonGenerator writer, GspBizEntityElement bizElement) {
//        //udt属性仅前端使用
//        if (Context.Mode == SerializerMode.Content)
//        {
//            return;
//        }

//        if (bizElement.getUnifiedDataType() == null) {
//            return;
//        }
//        // 属性名 UnifiedDataType
//        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.UnifiedDataType);
//
//        //属性值
//        SimpleDataTypeConverter serializer = new SimpleDataTypeConverter();
//        serializer.WriteJson(writer, bizElement.getUnifiedDataType(), null);
  }

  private void writeReqireCheckOccasion(JsonGenerator writer, GspBizEntityElement bizElement) {
    SerializerUtils.writePropertyName(writer, BizEntityJsonConst.RequiredCheckOccasion);
    String value;
    switch (bizElement.getRequiredCheckOccasion()) {
      case All:
        value = BizEntityEnumConst.RequiredCheckOccasionAll;
        break;
      case Save:
        value = BizEntityEnumConst.RequiredCheckOccasionSave;
        break;
      case Modify:
        value = BizEntityEnumConst.RequiredCheckOccasionModify;
        break;
      default:
        throw new RuntimeException("未定义的枚举值：" + bizElement.getRequiredCheckOccasion());
    }
    SerializerUtils.writePropertyValue_String(writer, value);
  }
}
