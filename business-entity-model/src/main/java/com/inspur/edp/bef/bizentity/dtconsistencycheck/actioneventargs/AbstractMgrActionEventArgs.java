/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs;


import com.inspur.edp.cef.designtime.api.dtconsistencycheck.AbstractDtEventArgs;

public abstract class AbstractMgrActionEventArgs extends AbstractDtEventArgs {

  public AbstractMgrActionEventArgs() {
  }

  protected String beId;
  protected String actionId;
  protected String metadataPath;

  public AbstractMgrActionEventArgs(String beId, String actionId, String metadataPath) {
    this.beId = beId;
    this.actionId = actionId;
    this.metadataPath = metadataPath;
  }

  public String getBeId() {
    return beId;
  }

  public void setBeId(String beId) {
    this.beId = beId;
  }

  public String getActionId() {
    return actionId;
  }

  public void setActionId(String actionId) {
    this.actionId = actionId;
  }

  public String getMetadataPath() {
    return metadataPath;
  }

  public void setMetadataPath(String metadataPath) {
    this.metadataPath = metadataPath;
  }
}
