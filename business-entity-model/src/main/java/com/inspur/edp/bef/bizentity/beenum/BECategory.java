/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.beenum;

/**
 * BE 实体类型
 */
public enum BECategory {
  /**
   * 标准BE
   */
  Standard(0),
  /**
   * DBE,用于元数据重用的嵌入式BE,需要依赖HO存在
   */
  DependentBusinessEntity(1),
  /**
   * 若干BE的公共部分抽象的大集合
   */
  MaxBusinessEntity(2);

  private final int intValue;
  private static java.util.HashMap<Integer, BECategory> mappings;

  private synchronized static java.util.HashMap<Integer, BECategory> getMappings() {
    if (mappings == null) {
      mappings = new java.util.HashMap<Integer, BECategory>();
    }
    return mappings;
  }

  BECategory(int value) {
    intValue = value;
    BECategory.getMappings().put(value, this);
  }

  public int getValue() {
    return intValue;
  }

  public static BECategory forValue(int value) {
    return getMappings().get(value);
  }
}
