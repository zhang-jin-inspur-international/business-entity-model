/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizCommonValdation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonValSerializer;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;

public class BizCommonValidationSerializer extends CommonValSerializer {

  public BizCommonValidationSerializer(boolean full) {
    super(full);
  }

  @Override
  protected void writeExtendCommonOpSelfProperty(JsonGenerator writer, CommonOperation info) {
    super.writeExtendCommonOpSelfProperty(writer, info);
    writeParameters(writer, (CommonValidation) info);
  }

  private void writeParameters(JsonGenerator writer, CommonValidation validation) {
    BizCommonValdation bizCommonValdation = (BizCommonValdation) validation;
    if (isFull || (bizCommonValdation.getParameterCollection() != null
        && bizCommonValdation.getParameterCollection().getCount() > 0)) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Parameters);
      //[
      SerializerUtils.WriteStartArray(writer);
      if (bizCommonValdation.getParameterCollection() != null
          && bizCommonValdation.getParameterCollection().getCount() > 0) {
        for (Object item : bizCommonValdation.getParameterCollection()) {
          new BizParameterSerializer(isFull).serialize((BizParameter) item, writer, null);
        }
      }
      //]
      SerializerUtils.WriteEndArray(writer);
    }
  }
}
