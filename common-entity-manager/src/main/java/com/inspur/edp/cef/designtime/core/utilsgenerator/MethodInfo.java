/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.core.utilsgenerator;

import java.util.ArrayList;
import java.util.List;

public final class MethodInfo {
    private JavaClassInfo belongClass;

    public MethodInfo()
{
}

private String methodName;
private TypeRefInfo returnType ;
private List<ParameterInfo> parameters=new ArrayList<>();
private List<JavaAccessModifier> accessModifiers=new ArrayList<>();
private List<String> methodBodies=new ArrayList<>();
private final String methodPrefix="    ";
private final String methodBodyPrefix="        ";

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public TypeRefInfo getReturnType() {
        return returnType;
    }

    public void setReturnType(TypeRefInfo returnType)
    {
        this.returnType=returnType;
    }

    public List<ParameterInfo> getParameters() {
        return parameters;
    }

    public List<JavaAccessModifier> getAccessModifiers() {
        return accessModifiers;
    }

    public List<String> getMethodBodies() {
        return methodBodies;
    }

    public void write(StringBuilder stringBuilder) {
        stringBuilder.append(methodPrefix);
        for (JavaAccessModifier modifier : getAccessModifiers())
            stringBuilder.append(modifier.toString() + " ");
        getReturnType().write(stringBuilder);
        stringBuilder.append(" ");
        stringBuilder.append(getMethodName()).append("(");
        int paramsCount = getParameters().size();
        for (int i = 0; i < paramsCount; i++)
        {
            ParameterInfo item=getParameters().get(i);
            item.write(stringBuilder);
            if(i<paramsCount-1)
                stringBuilder.append(",");
        }
        stringBuilder.append("){\n");

        for(String methodBody : getMethodBodies())
        {
            stringBuilder.append(methodBodyPrefix).append(methodBody).append("\n");
        }

        stringBuilder.append(methodPrefix).append("}");
    }
}
