/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.core.utilsgenerator;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.sun.xml.bind.v2.model.core.TypeRef;

class FieldSetMethodGenerator {
    private IGspCommonField field;
    private JavaClassInfo classInfo;
    private MethodInfo methodInfo = new MethodInfo();

    FieldSetMethodGenerator(IGspCommonField field, JavaClassInfo classInfo) {
        this.field = field;
        this.classInfo = classInfo;
    }

    private final String varData = "data";
    private final String varPropName = "propertyName";
    private final String varPropValue = "propertyValue";

    /*
     * 获取方法体生成逻辑，例（字符类型的属性）：
     *  super.setValue(data,propertyName,propertyValue);
     * */
//    @Override
//    protected Block buildMethodBody() {
//        Block block=ast.newBlock();
//        SuperMethodInvocation superMethodInvocation=ast.newSuperMethodInvocation();
//        superMethodInvocation.setName(ast.newSimpleName("setValue"));
//        superMethodInvocation.arguments().add(ast.newSimpleName(varData));
//        superMethodInvocation.arguments().add(ast.newSimpleName(varPropName));
//        superMethodInvocation.arguments().add(ast.newSimpleName(varData));
//
//        block.statements().add(ast.newExpressionStatement(superMethodInvocation));
//        return block;
//    }

    //    @Override
    protected String getMethodName() {
        return "set" + field.getLabelID();
    }

    //
//    @Override
    protected TypeRefInfo getReturnType() {
        return new TypeRefInfo(void.class);
    }
//
//    @Override
//    protected ArrayList<Modifier.ModifierKeyword> getAccessModifiers() {
//        ArrayList<Modifier.ModifierKeyword> list =new ArrayList<Modifier.ModifierKeyword>();
//        list.add(Modifier.ModifierKeyword.STATIC_KEYWORD);
//        list.add(Modifier.ModifierKeyword.PUBLIC_KEYWORD);
//        return list;
//    }

//    @Override
//    protected ArrayList<ParameterInfo> getParameterCollection() {
//
//        ArrayList<ParameterInfo> list = new ArrayList<>();
//        ParameterInfo paraData = new ParameterInfo();
//        paraData.setParamName(varData);
//        paraData.setParamType(new TypeInfo(IEntityData.class));
//        list.add(paraData);
//        ParameterInfo paraPropName = new ParameterInfo();
//        paraPropName.setParamName(varPropName);
//        paraPropName.setParamType(new TypeInfo(String.class));
//        list.add(paraData);
//        ParameterInfo paramValue = new ParameterInfo();
//        paraData.setParamName(varData);
//        paraData.setParamType(FieldGetMethodGenerator.getTypeInfo(field));
//        list.add(paraData);
//        return list;
//    }

    public void generate() {
        methodInfo.setMethodName(getMethodName());
        methodInfo.setReturnType(getReturnType());
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Public);
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Static);
        methodInfo.getParameters().add(new com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo(varData, new TypeRefInfo(IEntityData.class)));
        methodInfo.getParameters().add(new com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo(varPropValue, FieldGetMethodGenerator.getTypeInfo(field)));
        generateMethodBodies();
        classInfo.addMethodInfo(methodInfo);
    }

    private void generateMethodBodies() {
        classInfo.getImportInfos().addImportPackage("com.inspur.edp.cef.entity.entity.EntityDataUtils");
        methodInfo.getMethodBodies().add("EntityDataUtils.setValue(data,\""+field.getLabelID()+"\",propertyValue);");
    }
}
